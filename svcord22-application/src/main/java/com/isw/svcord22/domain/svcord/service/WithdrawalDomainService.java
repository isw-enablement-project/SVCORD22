package com.isw.svcord22.domain.svcord.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

import com.isw.svcord22.sdk.domain.svcord.entity.CreateServicingOrderProducerInput;
import com.isw.svcord22.sdk.domain.svcord.entity.CustomerReferenceEntity;
import com.isw.svcord22.sdk.domain.svcord.entity.Svcord22;
import com.isw.svcord22.sdk.domain.svcord.entity.UpdateServicingOrderProducerInput;
import com.isw.svcord22.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput;
import com.isw.svcord22.sdk.domain.svcord.service.WithdrawalDomainServiceBase;
import com.isw.svcord22.sdk.domain.svcord.type.ServicingOrderWorkResult;
import com.isw.svcord22.sdk.integration.facade.IntegrationEntityBuilder;
import com.isw.svcord22.sdk.integration.partylife.entity.RetrieveLoginInput;
import com.isw.svcord22.sdk.integration.partylife.entity.RetrieveLoginOutput;
import com.isw.svcord22.sdk.integration.paymord.entity.PaymentOrderInput;
import com.isw.svcord22.sdk.integration.paymord.entity.PaymentOrderOutput;
import com.isw.svcord22.domain.svcord.command.Svcord22Command;
import com.isw.svcord22.integration.partylife.service.RetrieveLogin;
import com.isw.svcord22.integration.paymord.service.PaymentOrder;
import com.isw.svcord22.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord22.sdk.domain.facade.Repository;

@Service
public class WithdrawalDomainService extends WithdrawalDomainServiceBase {

  private static final Logger log = LoggerFactory.getLogger(WithdrawalDomainService.class);

  @Autowired
  Svcord22Command servicingOrderProcedureCommand;

  @Autowired
  PaymentOrder paymentOrder;

  @Autowired
  RetrieveLogin retrieveLogin;

  @Autowired
  IntegrationEntityBuilder integrationEntityBuilder;

  public WithdrawalDomainService(DomainEntityBuilder entityBuilder, Repository repo) {

    super(entityBuilder, repo);

  }

  @NewSpan
  @Override
  public com.isw.svcord22.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput execute(
      com.isw.svcord22.sdk.domain.svcord.entity.WithdrawalDomainServiceInput withdrawalDomainServiceInput) {

    log.info("WithdrawalDomainService.execute()");
    // TODO: Add your service implementation logic

    RetrieveLoginInput retrieveInput = integrationEntityBuilder
        .getPartylife()
        .getRetrieveLoginInput()
        .setId("test1")
        .build();

    RetrieveLoginOutput retrieveLoginOutput = retrieveLogin.execute(retrieveInput);

    if (retrieveLoginOutput.getResult() != "SUCCESS") {
      return null;
    }

    CustomerReferenceEntity customerReferenceEntity = this.entityBuilder
        .getSvcord()
        .getCustomerReference().build();

    customerReferenceEntity.setAccountNumber(withdrawalDomainServiceInput.getAccountNumber());
    customerReferenceEntity.setAmount(withdrawalDomainServiceInput.getAmount());

    CreateServicingOrderProducerInput createServicingOrderProducerInput = this.entityBuilder
        .getSvcord()
        .getCreateServicingOrderProducerInput()
        .build();

    createServicingOrderProducerInput.setCustomerReference(customerReferenceEntity);

    Svcord22 svcord22 = servicingOrderProcedureCommand.createServicingOrderProducer(createServicingOrderProducerInput);

    PaymentOrderInput paymentOrderInput = integrationEntityBuilder.getPaymord().getPaymentOrderInput().build();
    paymentOrderInput.setAccountNumber(withdrawalDomainServiceInput.getAccountNumber());
    paymentOrderInput.setAmount(withdrawalDomainServiceInput.getAmount());
    paymentOrderInput.setExternalId("SVCORD22");
    paymentOrderInput.setExternalSerive("SVCORD22");
    paymentOrderInput.setPaymentType("CASH_WITHDRAWAL");

    PaymentOrderOutput paymentOrderOutput = paymentOrder.execute(paymentOrderInput);

    UpdateServicingOrderProducerInput updateServicingOrderProducerInput = this.entityBuilder.getSvcord()
        .getUpdateServicingOrderProducerInput().build();

    updateServicingOrderProducerInput.setUpdateID(svcord22.getId().toString());
    updateServicingOrderProducerInput
        .setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOrderOutput.getPaymentOrderResult()));

    servicingOrderProcedureCommand.updateServicingOrderProducer(svcord22, updateServicingOrderProducerInput);

    WithdrawalDomainServiceOutput withdrawalDomainServiceOutput = this.entityBuilder.getSvcord()
        .getWithdrawalDomainServiceOutput()
        .setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOrderOutput.getPaymentOrderResult()))
        .setTrasactionId(paymentOrderOutput.getTransactionId())
        .build();

    return withdrawalDomainServiceOutput;



    
  }

}
