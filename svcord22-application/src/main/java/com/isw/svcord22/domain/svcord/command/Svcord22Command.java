package com.isw.svcord22.domain.svcord.command;

import org.springframework.stereotype.Service;
import com.isw.svcord22.sdk.domain.svcord.command.Svcord22CommandBase;
import com.isw.svcord22.sdk.domain.svcord.entity.Svcord22Entity;
import com.isw.svcord22.sdk.domain.svcord.entity.ThirdPartyReferenceEntity;
import com.isw.svcord22.sdk.domain.svcord.type.ServicingOrderType;
import com.isw.svcord22.sdk.domain.svcord.type.ServicingOrderWorkProduct;
import com.isw.svcord22.sdk.domain.svcord.type.ServicingOrderWorkResult;
import com.isw.svcord22.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord22.sdk.domain.facade.Repository;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class Svcord22Command extends Svcord22CommandBase {

  private static final Logger log = LoggerFactory.getLogger(Svcord22Command.class);

  public Svcord22Command(DomainEntityBuilder entityBuilder, Repository repo) {
    super(entityBuilder, repo);
  }

  @Override
  public com.isw.svcord22.sdk.domain.svcord.entity.Svcord22 createServicingOrderProducer(
      com.isw.svcord22.sdk.domain.svcord.entity.CreateServicingOrderProducerInput createServicingOrderProducerInput) {

    log.info("Svcord22Command.createServicingOrderProducer()");
    // TODO: Add your command implementation logic

    Svcord22Entity servicingOrderProcedure = this.entityBuilder.getSvcord().getSvcord22().build();
    servicingOrderProcedure.setCustomerReference(createServicingOrderProducerInput.getCustomerReference());
    servicingOrderProcedure.setProcessStartDate(LocalDate.now());
    servicingOrderProcedure.setServicingOrderType(ServicingOrderType.PAYMENT_CASH_WITHDRAWALS);
    servicingOrderProcedure.setServicingOrderWorkDescription("CASH WITHDRAWALS");
    servicingOrderProcedure.setServicingOrderWorkProduct(ServicingOrderWorkProduct.PAYMENT);
    servicingOrderProcedure.setServicingOrderWorkResult(ServicingOrderWorkResult.PROCESSING);

    ThirdPartyReferenceEntity thirdPartyReference = new ThirdPartyReferenceEntity();
    thirdPartyReference.setId("test1");
    thirdPartyReference.setPassword("password");
    servicingOrderProcedure.setThirdPartyReference(thirdPartyReference);

    return repo.getSvcord().getSvcord22().save(servicingOrderProcedure);

  }

  @Override
  public void updateServicingOrderProducer(com.isw.svcord22.sdk.domain.svcord.entity.Svcord22 instance,
      com.isw.svcord22.sdk.domain.svcord.entity.UpdateServicingOrderProducerInput updateServicingOrderProducerInput) {

    log.info("Svcord22Command.updateServicingOrderProducer()");
    // TODO: Add your command implementation logic

    Svcord22Entity servicingOrderProcedure = this.repo.getSvcord().getSvcord22()
        .getReferenceById(Long.parseLong(updateServicingOrderProducerInput.getUpdateID()));
    servicingOrderProcedure.setCustomerReference(updateServicingOrderProducerInput.getCustomerReference());
    servicingOrderProcedure.setProcessEndDate(LocalDate.now());
    servicingOrderProcedure
        .setServicingOrderWorkResult(updateServicingOrderProducerInput.getServicingOrderWorkResult());
    servicingOrderProcedure.setThirdPartyReference(updateServicingOrderProducerInput.getThirdPartyReference());

    log.info(updateServicingOrderProducerInput.getUpdateID().toString());
    log.info(updateServicingOrderProducerInput.getCustomerReference().getAccountNumber());
    log.info(updateServicingOrderProducerInput.getCustomerReference().getAmount());

    this.repo.getSvcord().getSvcord22().save(servicingOrderProcedure);

  }

}
