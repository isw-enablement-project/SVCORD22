package com.isw.svcord22.integration.partylife.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import com.isw.svcord22.sdk.integration.partylife.entity.RetrieveLoginOutput;
import com.isw.svcord22.sdk.integration.partylife.partylife.model.LoginResponse;
import com.isw.svcord22.sdk.integration.partylife.partylife.provider.LoginApiPartylife;
import com.isw.svcord22.sdk.integration.partylife.service.LoginExecuteBase;
import com.isw.svcord22.sdk.integration.facade.IntegrationEntityBuilder;

@Service
public class LoginExecute extends LoginExecuteBase {

  private static final Logger log = LoggerFactory.getLogger(LoginExecute.class);

  @Autowired
private LoginApiPartylife partyLife;

  public LoginExecute(IntegrationEntityBuilder entityBuilder ) { 
    super(entityBuilder );
  }
  
  @NewSpan
  @Override
  public com.isw.svcord22.sdk.integration.partylife.entity.LoginExecuteOutput execute(com.isw.svcord22.sdk.integration.partylife.entity.LoginExecuteInput loginExecuteInput)  {

    log.info("LoginExecute.execute()");
    // TODO: Add your service implementation logic

   
    return null;
  }

}
