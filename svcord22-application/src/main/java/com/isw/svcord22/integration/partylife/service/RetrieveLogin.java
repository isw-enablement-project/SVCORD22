package com.isw.svcord22.integration.partylife.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import com.isw.svcord22.sdk.integration.partylife.entity.RetrieveLoginOutput;
import com.isw.svcord22.sdk.integration.partylife.partylife.model.LoginResponse;
import com.isw.svcord22.sdk.integration.partylife.partylife.provider.LoginApiPartylife;
import com.isw.svcord22.sdk.integration.partylife.service.RetrieveLoginBase;
import com.isw.svcord22.sdk.integration.facade.IntegrationEntityBuilder;

@Service
public class RetrieveLogin extends RetrieveLoginBase {

  private static final Logger log = LoggerFactory.getLogger(RetrieveLogin.class);
  
  // sdk에서 생성해준 integration의 것을 가져오는것...
  @Autowired
  private LoginApiPartylife partyLife;

  public RetrieveLogin(IntegrationEntityBuilder entityBuilder ) { 
    super(entityBuilder );
  }
  
  @NewSpan
  @Override
  public com.isw.svcord22.sdk.integration.partylife.entity.RetrieveLoginOutput execute(com.isw.svcord22.sdk.integration.partylife.entity.RetrieveLoginInput retrieveLoginInput)  {

    log.info("RetrieveLogin.execute()");
    // TODO: Add your service implementation logic

    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.set("accept", "application/json");
    httpHeaders.set("Content-Type", "application/json");  
  
    LoginResponse loginResponse = partyLife.getUserLogin(retrieveLoginInput.getId(), httpHeaders).getBody();
  
    RetrieveLoginOutput loginOuput = this.entityBuilder.getPartylife().getRetrieveLoginOutput().build();
    loginOuput.setResult(loginResponse.getResult().getValue());
  
    return loginOuput;
  
   
  }

}
